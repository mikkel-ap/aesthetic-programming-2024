let x = 0;
let speed = 1.5; // Adjust the speed of the ellipse
let direction = 1; // Direction of movement (1 for right, -1 for left)

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(60);
}

function draw() {
  background(30, 80);
  drawElements();
  drawCenteredText("THROBBER OF DOOM", 16, width / 2, height / 2);

  fill(255); // White fill color
  noStroke();
  ellipse(x, 100, 50, 50);

  // Move the ellipse according to the direction and speed
  x += speed * direction;

  // Check if the ellipse reaches the canvas boundaries
  if (x > width || x < 0) {
    // Reverse the direction when reaching the boundaries
    direction *= -1;
  }
}

function drawElements() {
  let num = 20;
  push();
  translate(width / 2, height / 2);
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  fill(225, 0, 0);
  ellipse(40, 160, 190, 30);
  pop();
  stroke(255, 255, 0, 18);

  let y = year();
  text('Current year: \n' + y, 720, 650);
}

function drawCenteredText(txt, fontSize, x, y) {
  textAlign(CENTER, CENTER);
  fill(255); // White text color
  textSize(fontSize);
  text(txt, x, y);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
