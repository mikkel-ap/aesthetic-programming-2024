
# THROBBER OF DOOM
  

This artwork represents an animation visualising a throbber. The throbber is made by a red ellipse quickly blinking 20 times within a given radius, representing the throbber. The throbber is made by a red shape on a black canvas. A circle is sliding towards the right side once upon loading, never to be seen again, giving the artwork an unsettling feeling. This is done by adding the current year, fast speed on the animation as well as a centred text area with the words: THROBBER OF DOOM.

![Screenshot af hjemmeside](https://i.ibb.co/qJYwRZW/Screenshot-2024-03-17-at-14-58-03.png)



Link to the website with the code is accessible [here](https://mikkel-ap.gitlab.io/aesthetic-programming-2024/miniX3)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->


You are welcome to view the full repository [here](https://gitlab.com/mikkel-ap/aesthetic-programming-2024/-/tree/main/miniX3)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

<br> 

<h1> Breakdown of the code 


This artwork represents a throbber. Essentially, it's a little animation that shows the user that something is processing. 

First off, we're starting the code off by adding a variable called x and setting it to 0. This x is going to be important for our throbber animation.

In the setup() function, we're setting up our drawing canvas. It's going to take up the whole window, and we're setting the frame rate to 60 frames per second. This will make it seem like the animation is happening faster.

In the draw() function, we're drawing the background with a slightly transparent gray color. The alpha value here controls the transparency, creating the unsettling black background. Then, we're calling two functions: drawElements() and drawCenteredText().

The drawElements() function is where the main animation happens. We're translating everything to the center of the canvas, then drawing a bunch of ellipses in a circular pattern. This gives that "throbber" effect.

The drawCenteredText() function is drawing text in the center of the canvas. In this case, it's drawing the text "THROBBER OF DOOM" with a font size of 16.

After that, we're drawing a white ellipse at position (x, 100) with a width and height of 50. This ellipse is going to move horizontally across the screen.

In the drawElements() function, we're setting up the values for drawing those ellipses in a circular pattern, making it seem like a throbber.

Below that, we're displaying the current year in the bottom right corner of the canvas.

Lastly, there's a function called windowResized(), which gets called whenever the window is resized. What it does is resize the canvas to fit the new window size.

// EDIT: The code has now introduced a loop, which makes the white ellipse move back and forth in a for-loop. The loop is possible due to 2 functions, speed and direction. By setting the direction to 1 as the argument, the ellipse will move to right with the given speed. In our loop, we set the boundaries: if x > width, x < 0, change direction to -1 and the animation will now go the other way. This will create the looping animation effect.

<br>

<h1> Reflection </h1>
This is the throbber of doom, which can seem a bit unsettling as the name indicates. This is due to the frustration which is usually connected to encountering when you have to wait for certain tasks. The throbber is intended as a feedback for the user, to inform them that the task has been understood, but the result is not yet visible. The throbber is known in many shapes and colours, making it possible to rethink it without boundaries. In this case, the throbber has been modified from a "regular" circular yellow throbber  with elements which was taught in Aesthetic Programming lectures, to the nerve breaking THROBBER OF DOOM.
  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[bookThrobberSketch lektion 9](https://brightspace.au.dk/d2l/le/lessons/123559/topics/1793620)


<br>

[Making Things Move slide 36](https://aarhusuniversitet-my.sharepoint.com/personal/au725046_uni_au_dk/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fau725046%5Funi%5Fau%5Fdk%2FDocuments%2FOneNote%20Uploads%2FLecture08%2D2%2Epdf&parent=%2Fpersonal%2Fau725046%5Funi%5Fau%5Fdk%2FDocuments%2FOneNote%20Uploads&ga=1)

<br>

[p5js.org/reference/circle](https://p5js.org/reference/#/p5/circle)

<br>

[p5js.org/reference/ellipse](https://p5js.org/reference/#/p5/ellipse)

<br>

[ChatGPT OpenAI ](https://chat.openai.com)



