
var bird;
var pipes = [];

function setup() {
  createCanvas(640, 480);
  bird = new Bird();
  pipes.push(new Pipe());
}

function draw() {
  background(0);

  for (var i = pipes.length-1 ; i >= 0; i--) { //For all the pipes in the array. Different Syntax to make the array delete itself from behind
    pipes[i].show(); //Show new pipe
    pipes[i].update(); //Move pipe

    if (pipes[i].hits(bird)) { //
      console.log("DEAD");
    }

    //Måske irrelevant
    if (pipes[i].offscreen()) {
      pipes.splice(i, 1); //Deletes the element in the array when off screen
    }
  }

  bird.update();
  bird.show();

  if (frameCount % 75 == 0) { // Every 75 frame a new pipe is added
    pipes.push(new Pipe()); // new pipes added
  }
}

function keyPressed() { //Making the bird jump
  if (key == ' ') {
    bird.up();
  }
}
