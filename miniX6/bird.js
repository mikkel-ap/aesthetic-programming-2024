
function Bird() { // Constructor func 
  this.y = height/2; // Centered Bird on Y-axis
  this.x = 64; // Spawn location

  this.gravity = 0.7; //Force drawing the bird down
  this.lift = -14; // Making the jump higher
  this.velocity = 0; //Increase of speed when falling

  this.show = function() { //Drawing the bird
    fill(255, 165, 0);
    ellipse(this.x, this.y, 32, 32);
  }

  this.up = function() { // Velocity making the bird go up
    this.velocity += this.lift; // 
  }

  this.update = function() {
    this.velocity += this.gravity;
    // this.velocity *= 0.9;
    this.y += this.velocity;

    if (this.y > height) { // Stopping the bird when falling of the screen
      this.y = height; 
      this.velocity = 0;
    }

    if (this.y < 0) { // Stopping the bird from going above the screen
      this.y = 0;
      this.velocity = 0;
    }

  }

}
