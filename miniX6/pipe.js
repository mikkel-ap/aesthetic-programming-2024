
function Pipe() { //Constructor function
  this.spacing = 175;
  this.top = random(height / 6, 3 / 4 * height);  // Position of top
  this.bottom = height - (this.top + this.spacing);
  this.x = width; // Starting of the edge of the screen
  this.w = 80;
  this.speed = 6;

  this.highlight = false;

  this.hits = function(bird) {
    if (bird.y < this.top || bird.y > height - this.bottom) { //Determines if the bird is hit by pipes
      if (bird.x > this.x && bird.x < this.x + this.w) { //Determines if the bird is within the goal area
        this.highlight = true;
        
        return true;
      }
    }
    this.highlight = false;
    return false;
  }

  this.show = function() { // Drawing the pipes
    fill(0, 255, 0); //Green by default
    if (this.highlight) {
      fill(255, 0, 0); //Red when hit
    }
    rect(this.x, 0, this.w, this.top); //Top pipe
    rect(this.x, height - this.bottom, this.w, this.bottom); //Bottom Pipe
  }

  this.update = function() {
    this.x -= this.speed; // The speed of pipes moving left
  }

  this.offscreen = function() {
    if (this.x < -this.w) { //Checks if the pipe is off the y-axis
      return true;
    } else {
      return false;
    }
  }


}