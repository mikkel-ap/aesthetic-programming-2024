# GCA / Generative Colourful Art
  

This artwork represents the way generative computational art can become aesthetically pleasing, by coding it with dynamic and organic-looking visual effects. The artwork is made by linear lines being drawn and randomly coloured depending on their location. The artwork runs without any direct interaction from the user and runs on it own. The artwork is using pseudo randomness with noise(), resulting in no pattern being repeated, despite the user refreshing the program. 

![Screenshot af hjemmeside](https://i.ibb.co/F4ghf78/Screenshot-2024-04-07-at-22-45-43.png)



Link to the website with the code is accessible [here](https://mikkel-ap.gitlab.io/aesthetic-programming-2024/miniX5)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->


You are welcome to view the full repository [here](https://gitlab.com/mikkel-ap/aesthetic-programming-2024/-/tree/main/miniX5)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

<br> 

<h1> Breakdown of the code 


We start off the coding by declaring two variables. totalLines and totalCircles are set to 0 and are made to keep track of the total number of lines and circles drawn on the canvas.

In the setup function, we draw the canvas and set its dimensions to match the window size by using the createCanvas() function. The background is made white by using the background function and giving it the RGB value 255. 

In the draw function, we include our conditional statements. Here, we control the drawing process. 

If totalLines is less than or equal to 3000, we call the drawLines() function to draw lines on the canvas. This function will be explained later in the breakdown. 

If totalLines exceeds 3000 and totalCircles is less than or equal 2000, the drawCircles function is called to draw circles on the canvas. This function will also be explained later in the breakdown

If both totalLines and totalCircles exceeds their limit, the canvas and count will be cleared, making the loop infinite. 

The drawLines function is creating the drawings of the random lines on the canvas. It iterates through a loop 10 times per frame, generating noise-based coordinates for the starting and ending points of each line. The color of each line is determined based on its position on the canvas. After drawing the lines, the totalLines counter is incremented by 10.

The drawCircles() function draws random circles on the canvas. Similar to the drawLines() function, it iterates through a loop 10 times per frame, generating random coordinates and properties for each circle. After drawing the circles, the totalCircles counter is incremented by 10.

Overall, this code controls the drawing process on the canvas, switching between drawing lines and circles based on the number of each type of shape drawn. Once the limit is reached for each type, the canvas is cleared, and the counters are reset. These rules and conditional statements make the artwork loop infinite. 



<br>

<h1> Reflection </h1>


The generative program uses two rules to create the artworkRule 1: Draw random lines on the canvas.
Rule 2: Change the color of the lines based on their position.

Conditional statements: 
1: If there is over 3000 lines in the canvas, draw 2000 circles.
2: If the canvas has over 2000 circles, clear the canvas and reset the count and repeat.

These boundaries make the artwork infinite, since the loop will keep repeating itself. The rules are playing a major role in the generative artwork, to make it seem random, but still controlled by boundaries. 

I was surprised positively by the theme of this miniX. I believe that setting up boundaries to auto-generate artwork is an interesting aspect to grasp, and even more difficult to distinguish whether the creativity is done by the computer or the person. 


### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->




Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 5.

<br>

Daniel Shiffman, “noise() vs random() - Perlin Noise and p5.js Tutorial,” (https://www.youtube.com/watch?v=YcdldZ1E9gU)

<br>

Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146 (https://10print.org/)

<br>

ChatGPT OpenAI (https://chat.openai.com)





