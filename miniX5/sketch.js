let totalLines = 0;
let totalCircles = 0;

function setup() {
  createCanvas(windowWidth, windowHeight); // Create a canvas
  background(255); // Set background color to white
}

function draw() {
  // Drawing lines
  if (totalLines <= 3000) {
    drawLines();
  }
  // Drawing circles
  else if (totalCircles <= 2000) {
    drawCircles();
  }
  // Clear canvas after drawing 1000 circles
  else {
    background(255); // Clear canvas
    totalCircles = 0; // Reset totalCircles counter
    totalLines = 0; // Reset totalLines counter
  }
}

// Function to draw random lines on the canvas
function drawLines() {
  for (let i = 0; i < 10; i++) { // Draw 10 lines per frame
    let x1 = noise(frameCount * 0.01 + i) * width; // Generate noise-based x-coordinate for starting point
    let y1 = noise(frameCount * 0.01 + i + 1000) * height; // Generate noise-based y-coordinate for starting point
    let x2 = noise(frameCount * 0.01 + i + 2000) * width; // Generate noise-based x-coordinate for ending point
    let y2 = noise(frameCount * 0.01 + i + 3000) * height; // Generate noise-based y-coordinate for ending point
    
    // Implement Rule 2: Change the color of the lines based on their position
    let lineColor = color(map(x1, 0, width, 0, 255), map(y1, 0, height, 0, 255), map(x2, 0, width, 0, 255), map(y2, 0, width, 0, 255)); // Color based on position
    
    stroke(lineColor); // Set stroke color
    line(x1, y1, x2, y2); // Draw line
  }
  totalLines += 10; // Increment total lines drawn
}

// Function to draw circles on the canvas
function drawCircles() {
  for (let i = 0; i < 10; i++) { // Draw 10 circles per frame
    let x = random(width); // Random x-coordinate for circle
    let y = random(height); // Random y-coordinate for circle
    let diameter = random(10, 50); // Random diameter for circle
    let circleColor = color(random(255), random(255), random(255)); // Random color for circle
    
    fill(circleColor); // Set fill color
    ellipse(x, y, diameter, diameter); // Draw circle
  }
  totalCircles += 10; // Increment total circles drawn
}
