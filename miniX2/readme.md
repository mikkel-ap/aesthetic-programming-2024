
# Emoji that can change mood
  
Short description: 
This artwork represents a canvas with a emoji and a button. The emoji is made by a yellow circle to visualise a head with 2 ellipses within, representing the eyes. The mouth is made by a red shape, which is able to change from looking surprised to looking normal. This is done by changing the shape of the mouth by pressing a button.

![Screenshot af hjemmeside](https://i.ibb.co/fFQG3M5/Screenshot-2024-03-11-at-12-38-51.png)



Link to the website with the code is accessible [here](https://mikkel-ap.gitlab.io/aesthetic-programming-2024/miniX2)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->


You are welcome to view the full repository [here](https://gitlab.com/mikkel-ap/aesthetic-programming-2024/-/tree/main/miniX2)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

<br> 

<h1> Breakdown of the code 

The code to visualise the artwork has been made by combining templates from the refencence list in p5. To see the examples, please visit the reference list further down the page. 

The program uses functions from the reference list such as circle, ellipses, createCanvas, fill and more to create the visualisation. 

To create a more interactive program, a button has been implemented to allow the user to change the emoji. 

This is possible due to the coding, where in the function setup line we create the canvas of the visualisation followed by a button using the function createButton. The mousePressed function is used to specify that the toggleShape function should be called when the button is pressed. The button knows whether to be a square or a rectangle by the command 'let isSquare = true;'

This variable is a boolean that determines whether the shape drawn will be a square or a rectangle. It is initially set to true, indicating that a square will be drawn.

In the function draw section, it sets the background color to gray, draws the face elements (head, eyes), and either a square or a rectangle based on the value of  isSquare.

In the last section, where we look at function toggleShape, this function is used when the button is pressed. It toggles the value of isSquare, changing the drawn shape between a square and a rectangle.

In summary, this code creates an interactive sketch where you can toggle between a square and a rectangle by clicking a button, and it displays a simple face on the canvas.

<br>
The universe of emojis has been expanding since the invention and has brought a lot of debate with it. In my opinion, things can easily get too complicated. This is why my emoji is simple and a bit goofy looking. It is not supposed to represent a special target group, but to make the user able to represent their feeling.
  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[p5js.org/reference/square](https://p5js.org/reference/#/p5/square)

<br>

[p5js.org/reference/circle](https://p5js.org/reference/#/p5/circle)

<br>

[p5js.org/reference/ellipse](https://p5js.org/reference/#/p5/ellipse)

<br>

[ChatGPT OpenAI ](https://chat.openai.com)

