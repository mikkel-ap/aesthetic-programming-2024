let isSquare = true;

function setup() {
  createCanvas(600, 300);

  // Create a button with the label "Change the mood"
  let shapeButton = createButton('Change the mood');
  shapeButton.position(10, height + 10);
  shapeButton.mousePressed(toggleShape);
}

function draw() {
  // Set the background color of the canvas to gray
  background(200);

  // Set the fill color to yellow
  fill('yellow');

  // Draw a circle as the face
  circle(125, 100, 200);

  // Now draw the ellipses as eyes
  ellipseMode(RADIUS);
  fill(255);
  ellipse(80, 50, 30, 30);
  ellipseMode(CENTER);
  fill(100);
  ellipse(85, 50, 30, 30);

  ellipseMode(RADIUS);
  fill(255);
  ellipse(175, 50, 30, 30);
  ellipseMode(CENTER);
  fill(100);
  ellipse(180, 50, 30, 30);

  // Set the fill color to red
  fill('red');

  // Draw either a square or a rectangle based on the isSquare variable
  if (isSquare) {
    square(100, 130, 55, 35, 20, 15);
  } else {
    rect(100, 130, 80, 35, 20, 15);
  }

}


function toggleShape() {
  // Toggle between square and rectangle when the button is clicked
  isSquare = !isSquare;
}
