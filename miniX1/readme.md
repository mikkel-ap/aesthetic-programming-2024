# Nattehimlens sorte huller
  
This artwork represents the black holes of the night sky and is a digital visualisation of a simplified universe, with a rotating box containing images of a fictional black hole. 

![Screenshot af hjemmeside](https://i.ibb.co/m81cGgD/baggrund.png "Visualisering af kunstværket")

![Screenshot af hjemmeside](https://i.ibb.co/fxvms1d/Screenshot-2024-03-03-at-13-43-22.png)
<br>

The code above is what has been used to create this artwork. 

The code has been modified from the original template to create this camera-looking effect. To see the example, please visit the reference list further down the page. 

The code starts up by using the keyword 'let', followed by naming the 3 parameters we wish to refer to. 

Then we follow up by declaring the function and naming it, then set it up with parameters. In the example below, we load the images which we wish to use as texture on the objects. This happens due to the coding in line 5-9, where we preload the images. 

Then in line 11-16, we draw the outline where we wish to add our canvas, where the visuals will be presented. I decided to create a big canvas with 1500 px on the y-parameter, followed by 600 px on the x-parameter. In the HTML file I filled the canvas with a black colour, to prevent any white colour from entering.

In the section starting from line 18, we start the line of code by using the keyword 'function' followed by the parameter draw. Then we value our background by entering 0 as the argument, followed by making the camera center this point from 3 different angles. 

In line 27, we change the camera angle from where we get the focus point. The coding makes it possible to move on the x-axis.

Now, drawing the upper and lower boxes:

Upper Box:
This is the where we enter values to affect the way of saving, texturing, boxing, and restoring. The upper box has a size of 35 units.

Lower box: 
Here we use effects of translating, rotating, texturing, and boxing. The lower box has the size of 275x275x10 units.

The camera choreographs its moves, swaying and twirling as it focuses on the center and glides along the x-axis.


<h1>
Reflective question:
What has been created?

</h1>
Nattehimlens sorte huller is an artwork created by Mikkel Tagesen Knudsen during the 2nd semester of the bachelor's program in Digital Design at Aarhus University.
  
  It its an artwork representing a fascination of the deep outer space, where everything is filled yet so empty. The image is visualising a fictional black hole as seen by the colours, but the artwork has a soothing feeling which creates relaxation for the viewer. 
<br>


Link to the website with the code is accessible [here](https://mikkel-ap.gitlab.io/aesthetic-programming-2024/miniX1)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>

You are welcome to view the full repository [here](https://gitlab.com/mikkel-ap/aesthetic-programming-2024.git)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[p5js.org/reference](https://p5js.org/reference/#/p5/createCamera)

<br>

[OpenAI ChatGPT](https://chat.openai.com)

<br>

[Billede af stjernehimmel](https://t3.ftcdn.net/jpg/04/17/49/40/360_F_417494070_VVtrnRQfse1X8dOQuUbM1OaaWrKQg882.jpg)

<br>

[Billede af fiktive sorte huller](https://static.wikia.nocookie.net/ultraverse/images/b/bd/The_Black_Hole.png/revision/latest?cb=20190901224959)
