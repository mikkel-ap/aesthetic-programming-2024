let camera;
let upperTextureImg;
let lowerTextureImg;

function preload() {
  // Load the images to be used as textures
  upperTextureImg = loadImage('sun.png');
  lowerTextureImg = loadImage('stjerner.jpg');
}

function setup() {
  createCanvas(1500, 600, WEBGL);
  background(0);
  camera = createCamera();
  describe('An example that creates a camera and moves it around the box.');
}

function draw() {
  background(0);

  // The camera will automatically
  // rotate to look at [0, 0, 0].
  camera.lookAt(0, 0, 0);

  // The camera will move on the
  // x-axis.
  camera.setPosition(sin(frameCount / 60) * 200, 20, 100);

  // Draw the upper lifted box with the upper texture
  push();
  texture(upperTextureImg);
  box(35);
  pop();

  // Draw the lower box with the lower texture
  push();
  translate(0, 50, 0);
  rotateX(HALF_PI);
  texture(lowerTextureImg);
  box(275, 275, 10);
  pop();
}
