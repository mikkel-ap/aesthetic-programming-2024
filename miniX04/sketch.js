let videoCapture;
let canvas;
let TrapButtonOn = true; // Variable to track if the button has been pressed
let buttonPressCount = 0; // Variable to store the count of button presses
var meldingen = 0;



function createVideoCapture() {
  videoCapture = createCapture(VIDEO);
  videoCapture.size(640, 480);
  videoCapture.hide();
}

let microphone;

function createAudioCapture() {
  microphone = new p5.AudioIn();
  microphone.start();
}

let cameraTracker;



function setup() {
  alert("Hello there.");
  alert("I'm a virus on your computer.");
  alert("But don't worry, i won't do anything bad. I'm just annoying.");
  alert("Please stop clicking me away.");
  alert("Hey, stop that!");
  alert("Okay, listen up you little punk, stop clicking!");
  alert("I said, STOP CLICKING!!!!");
  alert("AARRRGGGHHH, i can't stand you!");
  alert("Okay that's it! I'm deleting myself again.");
  alert("Bye.");
  alert("HEHE, you thought you could get rid of me that easily?!");
  alert("ENJOY THE POWER OF INFINITE POP-UPS!!!!! HEHEHEHEHEHE.");
  //noprotect
  while (meldingen <= 5) {
    alert("ACCEPT THE FOLLOWING AND I WILL BE GONE FOREVER!!! HEHEHE.");
    meldingen = meldingen + 1;
   }
  canvas = createCanvas(640, 480);
  createVideoCapture();
  createAudioCapture();

  setupRemoveVirusButton();
  setupCountBox(); // Call the function to set up the count box
  
}

function draw() {
  

  // Draw the videoCaptured with default resolution
  image(videoCapture, 0, 0, 640, 480);
  
}

function toggleTrapButton() {
  TrapButtonOn = !TrapButtonOn; // Toggle the state
}

function setupRemoveVirusButton() {
  // Styling the like button with CSS
  let RemoveVirusButton = createButton('REMOVE ALL VIRUS');
  RemoveVirusButton.position(195, 500);
  // Other styling properties...
  RemoveVirusButton.style("color", "#fff");
  RemoveVirusButton.style("padding", "10px 20px"); // Adjust padding as needed
  RemoveVirusButton.style("text-decoration", "none");
  RemoveVirusButton.style("font-size", "1.2em"); // Adjust font size as needed
  RemoveVirusButton.style("font-weight", "bold"); // Adjust font weight as needed
  RemoveVirusButton.style("border-radius", "50%"); // Round the corners to make a circle
  RemoveVirusButton.style("border", "2px solid #4c69ba"); // Add border
  RemoveVirusButton.style("background", "#4c69ba"); // Background color
  RemoveVirusButton.style("box-shadow", "2px 2px 5px rgba(0,0,0,0.2)"); // Add shadow
  RemoveVirusButton.style("cursor", "pointer"); // Change cursor to pointer on hover

  // Call toggleTrapButton function when the button is pressed
  RemoveVirusButton.mousePressed(function() {
    toggleTrapButton();
    updateButtonPressCount(); // Call a function to update the count
  });
}


function setupCountBox() {
  // Create a box to display the count
  countBox = createDiv('DATA IS NOW BEING COLLECTED..');
  countBox.position(10, height - 30); // Position the box at the bottom left corner
  countBox.style('color', '#fff'); // Text color
  countBox.style('font-size', '20px'); // Font size
}

function updateButtonPressCount() {
  buttonPressCount++;
  countBox.html('DATA COLLECTED: ' + buttonPressCount); // Update the count displayed in the box

  
    // Check if the button has been pressed 5 times
    if (buttonPressCount === 5) {
      displayPopup(); // Call function to display the popup
    }
  }
  
  function displayPopup() {
    // Display popup window with the specified text
    alert("Thank you for participating in our data collection, your data will now be sold to our partners such as Temu and Aliexpress, who will be sending you advertisements from now and until forever.");
  }
  
