
# Don't Be Fooled
  

This artwork represents the way you easily can be persuaded online. The artwork is made by allerts working as popup windows, a video capture and a press counter. The artwork starts off by showing popups, informing about a new dangerous virus on the computer. It informs that the user needs to accept the following to continue, and suddenly the web cam activates, giving the artwork an unsettling feeling. When the user tries to remove the virus 5 times, it suddenly opens a new pop up window again, informing the user that the data has now been successfully gathered and will be sold to associated partners and advertisements will forever be received. 

![Screenshot af hjemmeside](https://i.ibb.co/r411D07/Screenshot-2024-03-31-at-22-17-26.png)



Link to the website with the code is accessible [here](https://mikkel-ap.gitlab.io/aesthetic-programming-2024/miniX04)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->


You are welcome to view the full repository [here](https://gitlab.com/mikkel-ap/aesthetic-programming-2024/-/tree/main/miniX04)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

<br> 

<h1> Breakdown of the code 


First off, we're starting the code off by adding  variables, that we need later on in the process. 

In the setup() function, we're creating the popup windows by commanding with 'alert'. This creates the popup-window that informs the user. When the first alerts have been closed, a while-loop makes another pop up window appear 5 times, informing the user that it will soon be over, as long as they just accept the following. Lastly, we create the canvas and ask for permission to activate the video & audio and set up a counter & a button.

In the draw() function, we're drawing the video capture with the default resolution, which is 640x 480 px.

Further on, we create the function toggleTrapButton. This is where we toggle the state of the button, to see if it has been pressed.

We then setup the Remove Virus button and add CSS styling properties to give it the visual look and makes it look clickable, by adding the styling properties "cursor, pointer".

After that, we're data gathering by using MousePressed. This calls the toggleTrapButton function when the mouse presses the button and we're calling the updateButtonPressCount function to update the count. 

The count box is made to display the count. Simple CSS styling properties has also been included here to make the visualisation. 

We then create the function for the updateButtonPressCount, so that it shows DATA COLLECTED + the buttonPressCount function, to update the count display. 

We then create a if statement, where the program checks if the button has been pressed 5 times. If not, the last pop up window will keep on popping up when the button has been pressed. Afterwards, a new pop up will appear.

This is done by the function DisplayPopup, where it lastly will thank the user for the participation. 


<br>

<h1> Reflection </h1>
This artwork is a visualisation and over exaggeration of how your data suddenly can be gathered without your knowledge. This is due to the users usual behaviour online, where things happen fast. Data is being gathered all the time and sometimes the user is not even aware of it. This MiniX has provided me with an extra way of reflecting over the projects, and has given me motivation to dig a bit deeper into what's being produced. In this case, the artwork has been modified from a a facetracker which was introduced in Aesthetic Programming lectures, to this artwork "Don't Be Fooled"
  
 In the future, I would like to improve the way im writing the code, as I know some of the lines in the program is being repetitive and could be arranged in arrays instead, especially with the messages in pop up windows.  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->




Soon Winnie & Cox, Geoff, "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-120 (Chapter 4)

<br>

[FaceTrackerSlightlyRefactored lektion 11](https://brightspace.au.dk/d2l/le/lessons/123559/topics/1800793)

<br>

[P5 Popup Reference](https://editor.p5js.org/Corrie/sketches/UwUVr9Qzo)


<br>

[ChatGPT OpenAI ](https://chat.openai.com)



